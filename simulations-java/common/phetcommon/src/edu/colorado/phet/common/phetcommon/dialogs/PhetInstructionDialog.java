// Copyright 2002-2011, University of Colorado

package edu.colorado.phet.common.phetcommon.dialogs;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.lang.System;

import javax.swing.*;
import javax.swing.event.MouseInputAdapter;

import java.net.URISyntaxException;
import java.net.URI;
import java.io.IOException;
import java.io.File;
import edu.colorado.phet.common.phetcommon.util.FileUtils;
import edu.colorado.phet.common.phetcommon.PhetCommonConstants;
import edu.colorado.phet.common.phetcommon.application.ISimInfo;
import edu.colorado.phet.common.phetcommon.application.PaintImmediateDialog;
import edu.colorado.phet.common.phetcommon.application.PhetApplication;
import edu.colorado.phet.common.phetcommon.resources.PhetCommonResources;
import edu.colorado.phet.common.phetcommon.servicemanager.PhetServiceManager;
import edu.colorado.phet.common.phetcommon.simsharing.components.SimSharingJButton;
import edu.colorado.phet.common.phetcommon.view.HorizontalLayoutPanel;
import edu.colorado.phet.common.phetcommon.view.PhetLookAndFeel;
import edu.colorado.phet.common.phetcommon.view.SoftwareAgreementButton;
import edu.colorado.phet.common.phetcommon.view.VerticalLayoutPanel;
import edu.colorado.phet.common.phetcommon.view.util.HTMLUtils;
import edu.colorado.phet.common.phetcommon.view.util.HTMLUtils.InteractiveHTMLPane;
import edu.colorado.phet.common.phetcommon.view.util.SwingUtils;

import static edu.colorado.phet.common.phetcommon.simsharing.messages.UserComponents.aboutDialogCloseButton;
import static edu.colorado.phet.common.phetcommon.simsharing.messages.UserComponents.aboutDialogCreditsButton;

public class PhetInstructionDialog extends PaintImmediateDialog {

    // Copyright notice, not translated so no one messes with it, and so that we can easily change the date.
    private static final String COPYRIGHT_HTML_FRAGMENT =
            "<b>BilimLand Simulations</b><br>" +
            "Copyright &copy; 2004-2015 University of Colorado.<br>" +
            "Visit " + HTMLUtils.getPhetHomeHref();

    // translated strings
    private static final String TITLE = PhetCommonResources.getString( "Common.HelpMenu.AboutTitle" );
//    private static final String LOGO_TOOLTIP = PhetCommonResources.getString( "Common.About.WebLink" );
    private static final String SIM_VERSION = PhetCommonResources.getString( "Common.About.Version" );
    private static final String BUILD_DATE = PhetCommonResources.getString( "Common.About.BuildDate" );
    private static final String DISTRIBUTION = PhetCommonResources.getString( "Common.About.Distribution" );
    private static final String JAVA_VERSION = PhetCommonResources.getString( "Common.About.JavaVersion" );
    private static final String OS_VERSION = PhetCommonResources.getString( "Common.About.OSVersion" );
    private static final String CREDITS_BUTTON = PhetCommonResources.getString( "Common.About.CreditsButton" );
    private static final String CLOSE_BUTTON = PhetCommonResources.getString( "Common.choice.close" );

    private final ISimInfo config;

    /**
     * Constructs the dialog.
     *
     * @param phetApplication
     * @throws HeadlessException
     */
    public PhetInstructionDialog( PhetApplication phetApplication ) {
        this( phetApplication.getPhetFrame(), phetApplication.getSimInfo() );
    }

    /**
     * Constructs a dialog.
     *
     * @param config
     * @param owner
     */
    protected PhetInstructionDialog( Frame owner, ISimInfo config ) {
        super( owner );
        setResizable( true );

        this.config = config;

        setSize( 900, 900 );

        String titleString = config.getName();
        setTitle( TITLE + " " + titleString );

//        JPanel logoPanel = createLogoPanel();
        JPanel PDFPanel = createPDFPanel();
//        JPanel infoPanel = createInfoPanel( config );
        JPanel buttonPanel = createButtonPanel( config.isStatisticsFeatureIncluded() );

        VerticalLayoutPanel contentPanel = new VerticalLayoutPanel();
        contentPanel.setFillHorizontal();
        contentPanel.add( PDFPanel );
//        contentPanel.add( logoPanel );
//        contentPanel.add( new JSeparator() );
//        contentPanel.add( infoPanel );
        contentPanel.add( new JSeparator() );
        contentPanel.add( buttonPanel );
        setContentPane( contentPanel );

        pack();
        SwingUtils.centerInstructionInParent(this);
    }

    /*
     * Creates pane with instruction from PDF file
     */
    private JPanel createPDFPanel() {
//        String html = HTMLUtils.createStyledHTMLFromFragment( COPYRIGHT_HTML_FRAGMENT );
//        InteractiveHTMLPane pane = new InteractiveHTMLPane( html );
//        pane.setBackground( new JPanel().getBackground() );//see #1275

        HorizontalLayoutPanel Panel = new HorizontalLayoutPanel();
        Panel.setInsets(new Insets(10, 10, 10, 10)); // top,left,bottom,right
//        Panel.
//        PDFPanel.add(pane);

        String filename = "/Users/marcel/Documents/PHET/sims_from_phet/simulations-java/simulations/fractions/doc/build-a-fraction-implementation-notes.txt";
        try {
            File file = new File( filename );

            String txt_content = FileUtils.loadFileAsString( file );

            InteractiveHTMLPane txt_pane = new InteractiveHTMLPane( txt_content );
            txt_pane.setBackground(new JPanel().getBackground());//see #1275
            Panel.add(txt_pane);
        } catch ( IOException e ) {
            System.out.print("IOException error");
        }

        return Panel;
    }

    /*
     * Creates the panel that contains the logo and general copyright info.
     */
    private JPanel createLogoPanel() {

//        BufferedImage image = PhetCommonResources.getInstance().getImage( PhetLookAndFeel.PHET_LOGO_120x50 );
//        JLabel logoLabel = new JLabel( new ImageIcon( image ) );
//        logoLabel.setCursor( Cursor.getPredefinedCursor( Cursor.HAND_CURSOR ) );
//        logoLabel.setToolTipText( LOGO_TOOLTIP );
//        logoLabel.addMouseListener( new MouseInputAdapter() {
//            public void mouseReleased( MouseEvent e ) {
//                PhetServiceManager.showPhetPage();
//            }
//        } );

        String html = HTMLUtils.createStyledHTMLFromFragment( COPYRIGHT_HTML_FRAGMENT );
        InteractiveHTMLPane pane = new InteractiveHTMLPane( html );
        pane.setBackground( new JPanel().getBackground() );//see #1275

        HorizontalLayoutPanel logoPanel = new HorizontalLayoutPanel();
        logoPanel.setInsets( new Insets( 10, 10, 10, 10 ) ); // top,left,bottom,right
//        logoPanel.add( logoLabel );
        logoPanel.add( pane );

        return logoPanel;
    }

    /*
     * Creates the panel that displays info specific to the simulation.
     */
    private JPanel createInfoPanel( ISimInfo config ) {

//        String titleString = config.getName();
//        String versionString = config.getVersion().formatForAboutDialog();
//        String buildDate = config.getVersion().formatTimestamp();
//        String distributionTag = config.getDistributionTag();

        VerticalLayoutPanel infoPanel = new VerticalLayoutPanel();

        // Simulation title
//        JLabel titleLabel = new JLabel( titleString );
//        Font f = titleLabel.getFont();
//        titleLabel.setFont( new Font( f.getFontName(), Font.BOLD, f.getSize() ) );

        // Simulation version
//        JLabel versionLabel = new JLabel( SIM_VERSION + " " + versionString );

        // Build date
//        JLabel buildDateLabel = new JLabel( BUILD_DATE + " " + buildDate );

        // Distribution tag (optional)
//        JLabel distributionTagLabel = null;
//        if ( distributionTag != null && distributionTag.length() > 0 ) {
//            distributionTagLabel = new JLabel( DISTRIBUTION + " " + distributionTag );
//        }

        // Java runtime version
//        String javaVersionString = JAVA_VERSION + " " + System.getProperty( "java.version" );
//        JLabel javaVersionLabel = new JLabel( javaVersionString );

        // OS version
//        String osVersion = OS_VERSION + " " + System.getProperty( "os.name" ) + " " + System.getProperty( "os.version" );
//        JLabel osVersionLabel = new JLabel( osVersion );

        // layout
        int xMargin = 10;
        int ySpacing = 10;
        infoPanel.setInsets( new Insets( 0, xMargin, 0, xMargin ) ); // top,left,bottom,right
        infoPanel.add( Box.createVerticalStrut( ySpacing ) );
//        infoPanel.add( titleLabel );
        infoPanel.add( Box.createVerticalStrut( ySpacing ) );
//        infoPanel.add( versionLabel );
//        infoPanel.add( buildDateLabel );
//        if ( distributionTagLabel != null ) {
//            infoPanel.add( distributionTagLabel );
//        }
        infoPanel.add( Box.createVerticalStrut( ySpacing ) );
//        infoPanel.add( javaVersionLabel );
//        infoPanel.add( osVersionLabel );
        infoPanel.add( Box.createVerticalStrut( ySpacing ) );

        return infoPanel;
    }

    /*
    * Create the panel that contains the buttons.
    * The Credits button is added only if a credits file exists.
    */
    private JPanel createButtonPanel( boolean isStatisticsFeatureIncluded ) {

        // Software Use Agreement
//        JButton agreementButton = new SoftwareAgreementButton( this );

        // Credits
//        JButton creditsButton = new SimSharingJButton( aboutDialogCreditsButton, CREDITS_BUTTON );
//        creditsButton.addActionListener( new ActionListener() {
//            public void actionPerformed( ActionEvent e ) {
//                showCredits();
//            }
//        } );

        // Close
        JButton closeButton = new SimSharingJButton( aboutDialogCloseButton, CLOSE_BUTTON );
        getRootPane().setDefaultButton( closeButton );
        closeButton.addActionListener( new ActionListener() {
            public void actionPerformed( ActionEvent e ) {
                dispose();
            }
        } );

        // layout
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout( new FlowLayout() );
//        buttonPanel.add( agreementButton );
//        buttonPanel.add( creditsButton );
        buttonPanel.add( closeButton );

        return buttonPanel;
    }

    protected void showCredits() {
        new CreditsDialog( this, config.getProjectName() ).setVisible( true );
    }
}
